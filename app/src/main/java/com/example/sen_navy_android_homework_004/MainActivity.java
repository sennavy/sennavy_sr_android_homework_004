package com.example.sen_navy_android_homework_004;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btnGoDetail;
    FragmentManager manager;
    FragmentTransaction transaction;

    HomeLayoutFragment homeLayoutFragment;
    ListContainerFragment listContainerFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            manager = getSupportFragmentManager();
            transaction = manager.beginTransaction();
            homeLayoutFragment = new HomeLayoutFragment();
            transaction.replace(R.id.container, homeLayoutFragment);
            transaction.commit();
        }

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){

            manager = getSupportFragmentManager();
            transaction = manager.beginTransaction();

            homeLayoutFragment = new HomeLayoutFragment();
            listContainerFragment = new ListContainerFragment();

            transaction.replace(R.id.container, homeLayoutFragment);
            transaction.add(R.id.container, listContainerFragment);

            transaction.commit();
        }

    }
}
