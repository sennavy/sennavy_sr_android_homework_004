package com.example.sen_navy_android_homework_004;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class EmailListContainer extends AppCompatActivity {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_list_container);

        ArrayList<String> emails = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            emails.add("asnavy619@gmail.com");

            listView = findViewById(R.id.email_list_container);
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.email_container, R.id.email_container, emails);
            listView.setAdapter(arrayAdapter);
        }
    }
}
