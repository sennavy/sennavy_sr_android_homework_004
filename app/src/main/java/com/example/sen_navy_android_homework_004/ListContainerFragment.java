package com.example.sen_navy_android_homework_004;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

public class ListContainerFragment extends Fragment {
    ListView listView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_email_list_container, container, false);
        listView = view.findViewById(R.id.email_list_container);

        ArrayList<String> emails = new ArrayList<>();
        for (int i = 0; i < 50 ; i++){
            emails.add("asnavy619@gmail.com");
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),R.layout.email_container,R.id.email_container,emails);
        listView.setAdapter(arrayAdapter);

        return view;
    }
}
